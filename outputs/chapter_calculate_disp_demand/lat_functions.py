import glob
from collections import OrderedDict

import numpy as np
from math import sqrt

def load_all_lateral_spreads(ap):
    """
    This function loads all csv files as a dictionary

    :param ap: all_paths
    :return:
    """

    files = glob.glob(ap.LATERAL_SPREAD_DATA_PATH + "*.csv")  # Make a list of all csv files in folder

    split_point = ap.LATERAL_SPREAD_DATA_PATH[-10:]  # End of folder path used to split off file name
    split_point = split_point.replace("/", "\\")  # Needed since windows uses forward slash for path
    lsd = OrderedDict()  # Initialise a dictionary to hold lateral spread data

    for ffp in files:
        print(ffp)
        fname = ffp.split(split_point)[-1]
        fname = fname[:-4]  # removed .csv
        print(fname)
        data = np.loadtxt(ffp, skiprows=1, delimiter=",")
        dist = data[:, 0]
        u_g = data[:, 1]
        lsd[fname] = [dist, u_g]
    return lsd


def average(disp_demand):
    d = len(disp_demand)
    av = 0
    for i in disp_demand:
        av = av + i
        av = float(av) / d
        return av


def mediana(disp_demand):
    lista = sorted(disp_demand)
    n = len(disp_demand)
    centro = n // 2
    if (n % 2 == 1):
        m = lista[centro]
    else:
        m = (lista[centro - 1] + lista[centro]) / 2
    return m


def media(disp_demand):
    """Calcola il valore medio della sequenza."""
    return sum(disp_demand) / len(disp_demand)


def errore_semplice_medio(disp_demand):
    """Calcola l'errore semplice medio della sequenza."""
    med = media(disp_demand)
    return sum([abs(x - med) for x in disp_demand]) / len(disp_demand)

def varianza(disp_demand):
    """Calcola la varianza della sequenza."""
    med = media(disp_demand)
    return sum([(x - med) ** 2 for x in disp_demand]) / len(disp_demand)


def deviazione_standard(disp_demand):
    """Calcola la deviazione standard della sequenza."""
    return sqrt(varianza(disp_demand))


def sig2(disp_demand, av=None):
    ''' sig2(x,av=None) x is a vector '''
    d = len(disp_demand)
    if not av:
        av = average(disp_demand)
    s2 = 0
    for e in disp_demand:
        dev = e - av
        s2 = s2 + dev * dev
    if d > 1:
        s2 = float(s2) / (d - 1)
    return s2


if __name__ == '__main__':
    import all_paths

    load_all_lateral_spreads(all_paths)
