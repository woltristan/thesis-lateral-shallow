import numpy as np
import matplotlib.pyplot as plt

import all_paths as ap

data = np.loadtxt(ap.LATERAL_SPREAD_DATA_PATH + "Fig.5_2_Cristhcurch_central_business_cbd1_ch.csv", skiprows=1, delimiter=",")
dist = data[:, 0]
u_g = data[:, 1]

steps = np.arange(0, 110, 4.)  # create an array up to 200 metres at increments of 4m

u_g4 = np.interp(steps, dist, u_g)  # crea un vettore che ha 50 valori ottenuti interpolando tra i valori forniti in excel

disp_demand = u_g4[:-1] - u_g4[1:]

#  TODO: make this a nice plot
plt.hist(disp_demand)  # plot histogram of demand
plt.title("Histogram of the Occurrences of disp_demand")
plt.ylabel("Occurrences")
plt.xlabel("Delta_demand across 4m width [m]")
plt.show()
