import numpy as np
import matplotlib.pyplot as plt

from outputs.chapter_calculate_disp_demand import lat_functions as latf

import all_paths as ap
import scipy.stats

import math

def add_to_plot(sp, dds):



    # #
    # TODO: make another figure file and bin the data based on distance and then produce histogram (e.g. consider delta_demand distribution from 0-50m and compare with 50m-100m)
    #
    # av = latf.average(dds)

    # s2 = latf.sig2(dds, av)
    mediana = latf.mediana(dds)
    varianza = latf.varianza(dds)
    deviazione_standard = latf.deviazione_standard(dds)
    media=latf.media(dds)

    print("media=", media,"mediana = ", mediana, "deviazione_standard =", deviazione_standard)

    # produce fake random values based on defined distribution  # TODO: remove and use real data
    norm_dist_std_disp_demand = deviazione_standard  # va inserito il valore giusto
    median_disp_demand = media   # va inserito il valore giusto

    # plot histogram of real data
    sp.hist(dds, bins=50, normed=True)  # plot histogram of demand
    sp.set_title("Histogram of the Occurrences of disp_demand")
    sp.set_ylabel("Normalised Occurrences")
    sp.set_xlabel("Delta_demand across 4m width [m]")
    # plot the defined lognormal distribution function
    x = np.linspace(0, max(dds), 100)
    # print(x)
    # TODO: the inputs to this are wrong.
    pdf = scipy.stats.lognorm.pdf(x, norm_dist_std_disp_demand, loc=0, scale=median_disp_demand)
    plt.plot(x, pdf, label="original")


    # Fit a lognormal distribution to the data
    param = scipy.stats.lognorm.fit(dds)  # crea i parametri di cui si nutre la funzione fit, dovrebbero essere scatter=disersione, loc=?, media
    print(param)  # TODO: look up the actual meaning of these parameters in the scipy docs

    # Plot the fitted lognormal distribution function
    # pdf_fitted = scipy.stats.lognorm.pdf(x, param[0], loc=param[1], scale=param[2])  # fitted distribution
    # plt.plot(x, pdf_fitted, 'r-', label="scipy - fitted")


def create():
    bf, subplot = plt.subplots(ncols=1, figsize=(10, 4))

    lsd = latf.load_all_lateral_spreads(ap)
    dds = []
    steps = np.arange(0, 25, 4.)  # create an array up to 200 metres at increments of 4m
    for lat_name in lsd:
        lat_info = lsd[lat_name]
        dist = lat_info[0]
        u_g = lat_info[1]
        u_g4 = np.interp(steps, dist, u_g)
        disp_demand = u_g4[:-1] - u_g4[1:]
        for dd_val in disp_demand:
            dds.append(dd_val)
    dds = np.array(dds)
    add_to_plot(subplot, dds)
    print(dds)

    plt.legend()
    plt.show()



if __name__ == '__main__':
    create()
