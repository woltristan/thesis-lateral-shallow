import numpy as np
import matplotlib.pyplot as plt

import all_paths as ap

data = np.loadtxt(ap.LATERAL_SPREAD_DATA_PATH + "Fig.5_4_Avon_Loop_Point_bar_avnlp8_ch.csv", skiprows=1, delimiter=",")
dist = data[:, 0]
u_g = data[:, 1]

steps = np.arange(0, 300, 4.)  # create an array up to 200 metres at increments of 4m

u_g4 = np.interp(steps, dist,
                 u_g)  # crea un vettore che ha 50 valori ottenuti interpolando tra i valori forniti in excel

# print(u_g)
# print(dist)
# print(steps)
# print(u_g4)


# TODO: make this a nice plot
plt.plot(dist, u_g)
plt.plot(steps, u_g4, "o")
plt.title("Dallington_Loop_east_bank_2_dalLp16_ch(A)")
plt.ylabel("Ug_feb[m]")
plt.xlabel("Distance from Waterway, L [m]")
plt.grid(color='0.75', ls='--', lw=0.5)
plt.show()
