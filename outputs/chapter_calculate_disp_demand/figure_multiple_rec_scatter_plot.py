import numpy as np
import matplotlib.pyplot as plt

from outputs.chapter_calculate_disp_demand import lat_functions as latf

import all_paths as ap
import openpyxl

def add_to_plot(sps, dist, u_g, label):

    steps = np.arange(0, 200, 4.)  # create an array up to 200 metres at increments of 4m

    # crea un vettore che ha 50 valori ottenuti interpolando tra i valori forniti in excel
    u_g4 = np.interp(steps, dist, u_g)

    # TODO: make this a nice plot
    sps[0].plot(dist, u_g)
    sps[0].plot(steps, u_g4, "o", label=label)
    sps[0].set_title("Results of ground surveying")
    sps[0].set_ylabel("Ug_feb[m]")
    sps[0].set_xlabel("Distance from Waterway, L [m]")

    disp_demand = u_g4[:-1] - u_g4[1:]
    print(disp_demand)
    # vettore_disp_demand = []
    # nuovo_file = openpyxl.Workbook()
    # sheet = nuovo_file.get_sheet_by_name('Sheet')
    # for i in range(len(disp_demand)):
    #     vettore_disp_demand.append(disp_demand)
    #     sheet['A1'] = vettore_disp_demand
    #     sheet['A1'].value
    # nuovo_file.save('file_esempio.xlsx')



    #

    # TODO: make this a nice plot
    sps[1].plot(steps[:-1] + 2, disp_demand, "o", label= label)  # plot demand versus average distance from river
    sps[1].set_title("Demand versus average distance from river")
    sps[1].set_ylabel("Disp_demand [m]")
    sps[1].set_xlabel("Distance from Waterway, L [m]")


def create():
    bf, subplots = plt.subplots(ncols=2, figsize=(10, 4))
    lsd = latf.load_all_lateral_spreads(ap)
    for lat_name in lsd:
        lat_info = lsd[lat_name]
        disp = lat_info[0]
        u_g = lat_info[1]
        add_to_plot(subplots, disp, u_g, label=lat_name)
    plt.show()




if __name__ == '__main__':
    create()
