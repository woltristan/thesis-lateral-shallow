import numpy as np
import matplotlib.pyplot as plt

import all_paths as ap

data = np.loadtxt(ap.LATERAL_SPREAD_DATA_PATH + "Fig.5_4_Avon_Loop_Point_bar_avnlp8_ch.csv", skiprows=1, delimiter=",")
dist = data[:, 0]
u_g = data[:, 1]

steps = np.arange(0, 300, 4.)  # create an array up to 200 metres at increments of 4m

u_g4 = np.interp(steps, dist, u_g) #crea un vettore che ha 50 valori ottenuti interpolando tra i valori forniti in excel

disp_demand = u_g4[:-1] - u_g4[1:]
print(disp_demand)

# TODO: make this a nice plot
plt.plot(steps[:-1] + 2, disp_demand,"o")   # plot demand versus average distance from river
plt.title("Demand versus average distance from river")
plt.ylabel("Disp_demand [m]")
plt.xlabel("Distance from Waterway, L [m]")
plt.grid(color='0.75', ls='--', lw=0.5)
plt.show()