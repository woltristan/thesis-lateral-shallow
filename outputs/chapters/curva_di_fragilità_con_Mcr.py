import matplotlib.pyplot as plt
from matplotlib.ticker import MultipleLocator
from element_analysis import moment_theta as mt
from element_analysis import interaction_using_objects as interact
from element_analysis.models import Column, Soil
import numpy as np
from element_analysis import P_delta


import random


def calc_performance(theta_chord, theta_y, theta_ult, Vrcd,Vd, theta_cr):
    if theta_chord < theta_cr and Vrcd > Vd:
        return "no damage"
    if theta_cr <theta_chord < theta_y and Vrcd > Vd:
        return "moderate damage"
    elif theta_y < theta_chord < 3 / 4 * theta_ult  and Vrcd > Vd:
        return "severe damage"
    elif 3 / 4 * theta_ult <theta_chord < theta_ult and Vrcd > Vd:
        return "life safety"
    elif theta_chord > theta_ult and Vrcd > Vd:
        return "collapse"
    elif theta_chord > theta_ult and Vrcd < Vd:
        return "collapse"
    elif  Vrcd < Vd:
        return "collapse"


def column_and_soil_combinations():
    Perf = ["no damage", "moderate damage","severe damage", "life safety","collapse" ]
    n_demands = 40
    delta_demand = np.linspace(0.0,0.5, n_demands)
    PLss = []
    for k in range(n_demands):
        PLss.append({})
        for pp in Perf:
            PLss[k][pp] = []

    n_samples = 400  # column and soil combinations
    hs = np.linspace(300, 500, num=5)  # mm parameter variable
    bs = np.linspace(300,500,num=5)
    #shear_lenghts = np.linspace(3,6,num=5)
    # matrix_element = np.zeros((n_samples, 5))
    for j in range(n_samples):
        for k in range(len(hs)):
            # Define Moment-theta
            h = hs[k]
            b = bs[k]  # mm
            c = 40  # mm



            f_ck = 25 #np.absolute(random.normalvariate(25.68,8.64))  mPa mariniello ing.dei materiali resistenza media 25.68, deviazione standard 8.64
            f_yk = 450#random.normalvariate(325.4,23)   mPa mariniello ing.dei materiali tre tipi di acciai Aq42 media=325.4 deviazione standard= 23
            d_bar = 18
            n_bars = 5  # mm
            l_1 = 3  # m
            l_2 = 4  # m
            n_storey = 3
            q = 10  # kPa
            shear_lenght = random.uniform(3,5) # m
            spacing_stirrups = 150  # mm
            phi_stirrups = 6  # mm
            As_tot= mt.calc_As_tot(b,h)
            f_cd = mt.calc_f_cd(f_ck)
            f_yd = mt.calc_f_yd(f_yk)
            f_cm = mt.calc_f_cm(f_ck)
            Ab_s = mt.calc_Ab_s(d_bar, n_bars,As_tot)
            At_s = mt.calc_At_s(d_bar, n_bars,As_tot)
            f_ct = mt.calc_f_ct(f_ck)
            d = mt.calc_d(h, c)
            N = mt.calc_N(l_1,l_2,n_storey,q,f_ck,b,h)
            x = mt.calc_x(N, b, f_cd)
            E = mt.calc_E(f_cm)
            I = mt.calc_I(b, h)
            phi_y = mt.calc_phi_y(h, c, 0.00186)
            v = mt.calc_v(N, b, h, f_cd)
            omega_t = mt.calc_omega_t(At_s, f_yd, b, h, f_cd)
            omega_b = mt.calc_omega_b(At_s, f_yd, b, h, f_cd)
            b0 = mt.calc_b0(b, c, phi_stirrups, d_bar)
            h0 = mt.calc_h0(h, c, phi_stirrups, d_bar)
            alpha = mt.calc_efficience_stirrups(spacing_stirrups, b0, h0)
            area_stirrups = mt.calc_area_stirrups(phi_stirrups)
            pho_sx = mt.calc_geometrical_percentage_bars(area_stirrups, b0, spacing_stirrups)
            pho_tot = mt.calc_pho_longitudinal(At_s,Ab_s,b,h)
            moment_cr = mt.calc_M_cr(b,h,f_ct,N)
            M_cr = mt.calc_M_cr(b,h,f_ct,N)
            moment_y = mt.calc_M_y(f_cd, b, h, x, c, d, Ab_s, At_s, f_yd)
            theta_y = mt.calc_teta_y(phi_y, shear_lenght, h, f_yd, f_cd, d_bar)
            theta_ult = mt.calc_teta_u(v, omega_t, omega_b, f_cd, shear_lenght, h, alpha, pho_sx, f_yd)
            theta_cr = mt.calc_theta_cr(M_cr,shear_lenght,E,I)
            # print(theta_cr)
            # Define the foundation
            depth_found = random.uniform(1,2)
            height_found = random.uniform(0.5,1)
            width = random.uniform(0.5,1)
            # Define the Soil

            phi_soil = random.normalvariate(30,3)   # parameter variable
            gamma_soil = random.uniform(15,20)

            # Define Psoil
            phi_rad = np.radians(phi_soil)
            k_passive = P_delta.calc_k_passive(phi_rad)
            p_p = P_delta.calc_p_p(gamma_soil, k_passive, depth_found, height_found)

            P_max = P_delta.calc_P_max(p_p, height_found, width)
            N_spt = P_delta.calc_N_spt(phi_soil)
            k_soil = P_delta.calc_k_soil(N_spt, width)
            K_soil = P_delta.calc_K_soil(k_soil, width, depth_found)


            # Build the Soil Object
            soil = Soil()
            soil.k = K_soil
            soil.force_y = P_max
            soil.disp_y = P_max / K_soil

            # Build the Column Object
            column = Column()
            column.length = shear_lenght  # m
            column.moment_y = moment_y
            column.moment_cr= moment_cr
            column.theta_y = theta_y
            column.theta_ult = theta_ult
            column.force_y = column.moment_y / column.length
            column.disp_y = column.theta_y * column.length
            column.k = column.force_y / column.disp_y
            column.disp_u = theta_ult * column.length
            # matrix_element[k][0]=h
            # matrix_element[k][1] = phi_soil
            # matrix_element[k][2] = theta_y
            # matrix_element[k][3] = 3/4*theta_y
            # matrix_element[k][4] = theta_ult
            print(moment_cr, " m cr")
            print(moment_y,"m y")




            column_disps = []
            for k in range(len(delta_demand)):
                disp_demand = delta_demand[k]
                column_disp = interact.solve_column_displacement_array(column, soil, disp_demand, cmodel="strain-hardening", smodel="nonlinear")
                Vd = interact.solve_column_displacement_array_for_p_demand(column, soil, disp_demand, cmodel="strain-hardening", smodel="nonlinear")

                Vrcd = ((h - x) / (2 * shear_lenght* 10**3) * min(N*10**3, 0.55 * (b-c) * (h-c)* f_cd) + (1 - 0.05 * min((5, (column_disp / shear_lenght - theta_y) / theta_y))) * (0.16 * max(0.5 , 100 * pho_tot) * (1 - 0.16 * min((5, shear_lenght*10**3 / h))) * np.sqrt(f_cd) * (b-c) * (h-c) + (2*area_stirrups*f_yd*d) / spacing_stirrups))/10**3
                Vn1= (h - x) / (2 * shear_lenght* 10**3) * min(N*10**3, 0.55 * (b-c) * (h-c)* f_cd)
                Vn2 = ((1 - 0.05 * min((5, (column_disp / shear_lenght - theta_y) / theta_y))) * (0.16 * max(0.5 , 100 * pho_tot) * (1 - 0.16 * min((5, shear_lenght*10**3 / h))) * np.sqrt(f_cd) * (b-c) * (h-c)))
                Vn3 = (((1 - 0.05 * min((5, (column_disp / shear_lenght - theta_y) / theta_y)))*(2*area_stirrups*f_yd*d) / spacing_stirrups))
                # print(Vn1/1000)
                # print(Vn2/1000)
                # print(Vn3/1000)
                fattore_di_riduzione_degrado = 1 - 0.05 * min((5, (column_disp / shear_lenght - theta_y) / theta_y))
                # print(fattore_di_riduzione_degrado)
                # print( N, " sforzo normale")
                # print(h, " altezza sezioe")
                # print(b, " base sezione")
                # print(Vd, " taglio agente")
                # print(Vrcd, "taglio resistente")
                # print(P_max, " taglio massimo")
                if Vd > Vrcd:
                    print(Vd, " caso rottura per taglio resistente")
                    print(Vrcd ," taglio resistente")

                column_disps.append(column_disp)
                pl = calc_performance(column_disp / shear_lenght, theta_y, theta_ult, Vrcd , Vd, theta_cr)
                #print(k, pl)
                PLss[k][pl].append(column_disp)
                #print(pl)
            plt.plot(delta_demand, column_disps, label="Disp_demands- Column_Disp %i-%i" % (k, j))

    plt.show()
    # per fare la validazione creo 2 vettori cosi li plotto assieme
    delta_demand_validation_curvasopra = [0.014173174,0.032754538,0.044237113,0.057489586,0.066309457,0.08221243,0.09458257,0.11313778,0.12727608,0.13611512,0.14671885,0.16706314,0.18828978,0.20598532,0.23253036,0.25288687,0.28652723,0.31485615,0.34584427,0.37418713,0.39720806,0.42023426]
    probabilità_curva_sopra = [9.63E-05,0.036920205,0.09510279,0.15635553,0.22369443,0.29719773,0.3523282,0.43502393,0.4962827,0.52998227,0.5759263,0.62805283,0.68630165,0.7231195,0.7752882,0.80600786,0.84293413,0.87370795,0.90144175,0.90775067,0.9262559,0.9355868]
    delta_demand_validation_curvasotto = [0.057578516,0.1248976,0.15234418,0.17447233,0.20987387,0.24172863,0.2744605,0.30189138,0.33020287,0.3594019,0.38594696,0.38860268,0.41957336,0.44966347]
    probabilità_curva_sotto = [3.91E-04,0.006965054,0.03161665,0.062348373,0.11763538,0.17901453,0.25569034,0.30786502,0.3692201,0.4275231,0.47969177,0.48276797,0.541083,0.59021765]
    # validazione tramite risultati Hazus
    delta_demand_validation_hazus_sopra = [0.001680665,0.089004755,0.16457516,0.23930601,0.30480026,0.448382]
    probabilità_curva_sopra_hazus = [0.003532891,0.027667696,0.051895123,0.07612917,0.096889870,0.13831201]
    delta_demand_validation_hazus_sotto = [0.001683809,0.04967474,0.25342829,0.4504466]
    probabilità_curva_sotto_hazus = [0.003559381,0.099682580,0.501998840,0.8900776]
    ppl_0 = []
    ppl_1 = []
    ppl_2 = []
    ppl_3 = []
    ppl_4 = []
    for k in range(n_demands):
        total_sample = len(PLss[k]["collapse"]) + len(PLss[k]["life safety"]) + len(PLss[k]["severe damage"]) + len(PLss[k]["moderate damage"])+ len(PLss[k]["no damage"])
        ppl_4.append(len(PLss[k]["collapse"]) / total_sample)
        ppl_3.append((len(PLss[k]["collapse"]) + len(PLss[k]["life safety"]) ) / total_sample)
        ppl_2.append((len(PLss[k]["collapse"]) + len(PLss[k]["life safety"])+ len(PLss[k]["severe damage"]))/total_sample)
        ppl_1.append((len(PLss[k]["collapse"]) + len(PLss[k]["life safety"]) + len(PLss[k]["severe damage"]) + len(PLss[k]["moderate damage"]))/ total_sample)
        ppl_0.append((len(PLss[k]["collapse"]) + len(PLss[k]["life safety"]) + len(PLss[k]["severe damage"]) + len(PLss[k]["moderate damage"]) +len(PLss[k]["no damage"])) / total_sample)
    # plt.plot(delta_demand_validation_curvasopra, probabilità_curva_sopra, label="limiting tollerable settlement")
    # plt.plot(delta_demand_validation_curvasotto, probabilità_curva_sotto, label="limiting intollerable settlement")
    # plt.plot(delta_demand_validation_hazus_sopra, probabilità_curva_sopra_hazus, label="hazus sopra")
    # plt.plot(delta_demand_validation_hazus_sotto, probabilità_curva_sotto_hazus, label="hazus sotto")
    plt.plot(delta_demand, ppl_4, label="collapse")
    plt.plot(delta_demand, ppl_3, label="life safety")
    plt.plot(delta_demand, ppl_2, label="severe damage")
    plt.plot(delta_demand, ppl_1, label="moderate damage")
    # plt.plot(delta_demand, ppl_1, label="no danno")
    plt.title("Fragility Curve")
    plt.grid(color='0.75', ls='--', lw=0.5)
    plt.xlabel('Delta Demand [m]')
    plt.ylabel('Damage Probability [-]')
    plt.ylim(-0.05,1.05)
    plt.xlim(-0.05, 0.45)
    plt.legend()
    plt.show()
    # per adesso non riesco a computare la richiesta di spostamento tenendo conto del trilineare.

column_and_soil_combinations()