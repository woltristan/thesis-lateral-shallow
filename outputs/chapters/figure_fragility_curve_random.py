import matplotlib.pyplot as plt
from matplotlib.ticker import MultipleLocator
from element_analysis import moment_theta as mt
from element_analysis import interaction_using_objects as interact
from element_analysis.models import Column, Soil
import numpy as np
from element_analysis import P_delta





def calc_performance(theta_chord, theta_y, theta_ult):
    if theta_chord < theta_y:
        return "no damage"
    elif theta_y < theta_chord < 3 / 4 * theta_ult:
        return "life safety"
    elif theta_chord > 3 / 4 * theta_ult:
        return "collapse"


def column_and_soil_combinations():
    Perf = ["no damage", "life safety", "collapse"]
    n_demands = 40
    delta_demand = np.linspace(0.0,0.5, n_demands)
    PLss = []
    for k in range(n_demands):
        PLss.append({})
        for pp in Perf:
            PLss[k][pp] = []

    n_samples = 400  # column and soil combinations
    hs = np.linspace(400, 400, num=1)  # mm parameter variable
    bs = np.linspace(400,400,num=1)
    matrix_element = np.zeros((n_samples, 5))
    for j in range(n_samples):
        for k in range(len(hs)):
            # Define Moment-theta
            h = hs[k]
            b = bs[k]  # mm
            c = 40  # mm

            f_ck = 25  # mPa
            f_yk = 450  # mPa
            d_bar = 16 + 12 * np.random.rand()
            n_bars = 4 + 4 * np.random.rand()  # mm
            l_1 = 2.5 + 2.5 * np.random.rand()  # m
            l_2 = 3 + 2 * np.random.rand()  # m
            n_storey = 2 + 3 *np.random.rand()
            q = 10 + 20 * np.random.rand()  # kPa
            shear_lenght = 2.5 + 2.5 *np.random.rand()  # m
            spacing_stirrups = 150  # mm
            phi_stirrups = 8  # mm

            f_cd = mt.calc_f_cd(f_ck)
            f_yd = mt.calc_f_yd(f_yk)
            f_cm = mt.calc_f_cm(f_ck)
            As_tot = mt.calc_As_tot(b, h)
            Ab_s = mt.calc_Ab_s(d_bar, n_bars, As_tot)
            At_s = mt.calc_At_s(d_bar, n_bars, As_tot)
            f_ct = mt.calc_f_ct(f_ck)
            d = mt.calc_d(h, c)
            N = mt.calc_N(l_1, l_2, n_storey, q, f_ck, b, h)
            x = mt.calc_x(N, b, f_cd)
            E = mt.calc_E(f_cm)
            I = mt.calc_I(b, h)
            phi_y = mt.calc_phi_y(h, c, 0.00186)
            v = mt.calc_v(N, b, h, f_cd)
            omega_t = mt.calc_omega_t(At_s, f_yd, b, h, f_cd)
            omega_b = mt.calc_omega_b(At_s, f_yd, b, h, f_cd)
            b0 = mt.calc_b0(b, c, phi_stirrups, d_bar)
            h0 = mt.calc_h0(h, c, phi_stirrups, d_bar)
            alpha = mt.calc_efficience_stirrups(spacing_stirrups, b0, h0)
            area_stirrups = mt.calc_area_stirrups(phi_stirrups)
            pho_sx = mt.calc_geometrical_percentage_bars(area_stirrups, b0, spacing_stirrups)

            moment_y = mt.calc_M_y(f_cd, b, h, x, c, d, Ab_s, At_s, f_yd)
            theta_y = mt.calc_teta_y(phi_y, shear_lenght, h, f_yd, f_cd, d_bar)
            theta_ult = mt.calc_teta_u(v, omega_t, omega_b, f_cd, shear_lenght, h, alpha, pho_sx, f_yd)

            # Define the foundation
            depth_found = 1 + 1. * np.random.rand()
            height_found = 0.5 + 0.3 * np.random.rand()
            width = 0.5 + 1 + np.random.rand()
            # Define the Soil
            phi_soil = 27. + 8. * np.random.randn()  # parameter variable
            gamma_soil = 16 + 4 * np.random.randn()

            # Define Psoil
            phi_rad = np.radians(phi_soil)
            k_passive = P_delta.calc_k_passive(phi_rad)
            p_p = P_delta.calc_p_p(gamma_soil, k_passive, depth_found, height_found)

            P_max = P_delta.calc_P_max(p_p, height_found, width)
            N_spt = P_delta.calc_N_spt(phi_soil)
            k_soil = P_delta.calc_k_soil(N_spt, width)
            K_soil = P_delta.calc_K_soil(k_soil, width, depth_found)

            # Build the Soil Object
            soil = Soil()
            soil.k = K_soil
            soil.force_y = P_max
            soil.disp_y = P_max / K_soil

            # Build the Column Object
            column = Column()
            column.length = shear_lenght  # m
            column.moment_y = moment_y
            column.theta_y = theta_y
            column.theta_ult = theta_ult
            column.force_y = column.moment_y / column.length
            column.disp_y = column.theta_y * column.length
            column.k = column.force_y / column.disp_y
            column.disp_u = theta_ult * column.length
            matrix_element[k][0]=h
            matrix_element[k][1] = phi_soil
            matrix_element[k][2] = theta_y
            matrix_element[k][3] = 3/4*theta_y
            matrix_element[k][4] = theta_ult

            column_disps = []
            for k in range(len(delta_demand)):
                disp_demand = delta_demand[k]
                column_disp = interact.solve_column_displacement_array(column, soil, disp_demand, cmodel="strain-hardening", smodel="nonlinear")
                #print(column_disp)
                column_disps.append(column_disp)
                pl = calc_performance(column_disp / shear_lenght, theta_y, theta_ult)
                #print(k, pl)
                PLss[k][pl].append(column_disp)
                #print(pl)
            plt.plot(delta_demand, column_disps, label="Disp_demands- Column_Disp %i-%i" % (k, j))
            plt.title("Disp Demands - Column Disp")
            plt.xlabel('Disp Demands [m]')
            plt.ylabel('Column_Disp [m]')


    plt.show()


    ppl_1 = []
    ppl_2 = []
    ppl_3 = []
    for k in range(n_demands):
        total_sample = len(PLss[k]["collapse"]) + len(PLss[k]["life safety"]) + len(PLss[k]["no damage"])
        ppl_3.append(len(PLss[k]["collapse"]) / total_sample)
        ppl_2.append((len(PLss[k]["collapse"]) + len(PLss[k]["life safety"]))/total_sample)
        ppl_1.append((len(PLss[k]["collapse"]) + len(PLss[k]["life safety"]) + len(PLss[k]["no damage"])) / total_sample)
    plt.plot(delta_demand, ppl_3, label="collapse")
    plt.plot(delta_demand, ppl_2, label="life safety")
    plt.plot(delta_demand, ppl_1, label="no damage")
    plt.title("Fragility curves")
    plt.xlabel('Disp Demands [m]')
    plt.ylabel('Probability collapse [%]')
    plt.legend()
    plt.show()

column_and_soil_combinations()

