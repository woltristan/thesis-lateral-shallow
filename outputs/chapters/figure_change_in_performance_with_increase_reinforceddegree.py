import matplotlib.pyplot as plt
from matplotlib.ticker import MultipleLocator
from element_analysis import moment_theta as mt
from element_analysis import interaction_using_objects as interact
from element_analysis.models import Column, Soil
import numpy as np
from element_analysis import P_delta

import random

def funzione(variabile):

            # Define Moment-theta
            h = 400
            b = 400 # mm
            c = 40  # mm

            f_ck = 25  # mPa
            f_yk = 450  # mPa
            d_bar = 18
            n_bars = 5  # mm
            l_1 = 3  # m
            l_2 = 4  # m
            n_storey = 3
            q = 10  # kPa
            shear_lenght = 4.  # m
            spacing_stirrups = 150  # mm
            phi_stirrups = 8  # mm

            f_cd = mt.calc_f_cd(f_ck)
            f_yd = mt.calc_f_yd(f_yk)
            f_cm = mt.calc_f_cm(f_ck)
            As_tot = variabile* mt.calc_As_tot(b, h)
            Ab_s = mt.calc_Ab_s(d_bar, n_bars, As_tot)
            At_s = mt.calc_At_s(d_bar, n_bars, As_tot)
            f_ct = mt.calc_f_ct(f_ck)
            d = mt.calc_d(h, c)
            N = mt.calc_N(l_1, l_2, n_storey, q, f_ck, b, h)
            x = mt.calc_x(N, b, f_cd)
            E = mt.calc_E(f_cm)
            I = mt.calc_I(b, h)
            phi_y = mt.calc_phi_y(h, c, 0.00186)
            v = mt.calc_v(N, b, h, f_cd)
            omega_t = mt.calc_omega_t(At_s, f_yd, b, h, f_cd)
            omega_b = mt.calc_omega_b(At_s, f_yd, b, h, f_cd)
            b0 = mt.calc_b0(b, c, phi_stirrups, d_bar)
            h0 = mt.calc_h0(h, c, phi_stirrups, d_bar)
            alpha = mt.calc_efficience_stirrups(spacing_stirrups, b0, h0)
            area_stirrups = mt.calc_area_stirrups(phi_stirrups)
            pho_sx = mt.calc_geometrical_percentage_bars(area_stirrups, b0, spacing_stirrups)
            pho_tot = mt.calc_pho_longitudinal(At_s, Ab_s, b, h)
            moment_y = mt.calc_M_y(f_cd, b, h, x, c, d, Ab_s, At_s, f_yd)
            theta_y = mt.calc_teta_y(phi_y, shear_lenght, h, f_yd, f_cd, d_bar)
            theta_ult = mt.calc_teta_u(v, omega_t, omega_b, f_cd, shear_lenght, h, alpha, pho_sx, f_yd)

            # Define the foundation
            depth_found = 1
            height_found = 0.3
            width = 2 * height_found + h/1000
            # Define the Soil
            phi_soil = 30
            gamma_soil = 17

            # Define Psoil
            phi_rad = np.radians(phi_soil)
            k_passive = P_delta.calc_k_passive(phi_rad)
            p_p = P_delta.calc_p_p(gamma_soil, k_passive, depth_found, height_found)

            P_max = P_delta.calc_P_max(p_p, height_found, width)
            N_spt = P_delta.calc_N_spt(phi_soil)
            k_soil = P_delta.calc_k_soil(N_spt, width)
            K_soil = P_delta.calc_K_soil(k_soil, width, depth_found)

            # Build the Soil Object
            soil = Soil()
            soil.k = K_soil
            soil.force_y = P_max
            soil.disp_y = P_max / K_soil

            # Build the Column Object
            column = Column()
            column.length = shear_lenght  # m
            column.moment_y = moment_y
            column.theta_y = theta_y
            column.theta_ult = theta_ult
            column.force_y = column.moment_y / column.length
            column.disp_y = column.theta_y * column.length
            column.k = column.force_y / column.disp_y
            column.disp_u = theta_ult * column.length
            column_disps = []
            n_demands = 40
            delta_demand = np.linspace(0.0, 0.5, n_demands)
            for k in range(len(delta_demand)):
                disp_demand = delta_demand[k]
                column_disp = interact.solve_column_displacement_array(column, soil, disp_demand,
                                                                       cmodel="strain-hardening", smodel="nonlinear")
                column_disps.append(column_disp)
            print(column_disp)
            return column_disps

def run():
    # Define Moment-theta
    h = 300
    b = 300  # mm
    c = 40  # mm

    f_ck = 25  # mPa
    f_yk = 450  # mPa
    d_bar = 18
    n_bars = 5  # mm
    l_1 = 3  # m
    l_2 = 4  # m
    n_storey = 3
    q = 10  # kPa
    shear_lenght = 4.  # m
    spacing_stirrups = 150  # mm
    phi_stirrups = 8  # mm

    f_cd = mt.calc_f_cd(f_ck)
    f_yd = mt.calc_f_yd(f_yk)
    f_cm = mt.calc_f_cm(f_ck)
    As_tot = mt.calc_As_tot(b, h)
    Ab_s = mt.calc_Ab_s(d_bar, n_bars, As_tot)
    At_s = mt.calc_At_s(d_bar, n_bars, As_tot)
    f_ct = mt.calc_f_ct(f_ck)
    d = mt.calc_d(h, c)
    N = mt.calc_N(l_1, l_2, n_storey, q, f_ck, b, h)
    x = mt.calc_x(N, b, f_cd)
    E = mt.calc_E(f_cm)
    I = mt.calc_I(b, h)
    phi_y = mt.calc_phi_y(h, c, 0.00186)
    v = mt.calc_v(N, b, h, f_cd)
    omega_t = mt.calc_omega_t(At_s, f_yd, b, h, f_cd)
    omega_b = mt.calc_omega_b(At_s, f_yd, b, h, f_cd)
    b0 = mt.calc_b0(b, c, phi_stirrups, d_bar)
    h0 = mt.calc_h0(h, c, phi_stirrups, d_bar)
    alpha = mt.calc_efficience_stirrups(spacing_stirrups, b0, h0)
    area_stirrups = mt.calc_area_stirrups(phi_stirrups)
    pho_sx = mt.calc_geometrical_percentage_bars(area_stirrups, b0, spacing_stirrups)
    pho_tot = mt.calc_pho_longitudinal(At_s, Ab_s, b, h)
    moment_y = mt.calc_M_y(f_cd, b, h, x, c, d, Ab_s, At_s, f_yd)
    theta_y = mt.calc_teta_y(phi_y, shear_lenght, h, f_yd, f_cd, d_bar)
    theta_ult = mt.calc_teta_u(v, omega_t, omega_b, f_cd, shear_lenght, h, alpha, pho_sx, f_yd)

    # Define the foundation
    depth_found = 1
    height_found = 0.5
    width = 2 * height_found + h/1000
    print(width," sezione fond")
    # Define the Soil
    phi_soil = 40
    gamma_soil = 18

    # Define Psoil
    phi_rad = np.radians(phi_soil)
    k_passive = P_delta.calc_k_passive(phi_rad)
    p_p = P_delta.calc_p_p(gamma_soil, k_passive, depth_found, height_found)

    P_max = P_delta.calc_P_max(p_p, height_found, width)
    N_spt = P_delta.calc_N_spt(phi_soil)
    k_soil = P_delta.calc_k_soil(N_spt, width)
    K_soil = P_delta.calc_K_soil(k_soil, width, depth_found)

    # Build the Soil Object
    soil = Soil()
    soil.k = K_soil
    soil.force_y = P_max
    soil.disp_y = P_max / K_soil

    # Build the Column Object
    column = Column()
    column.length = shear_lenght  # m
    column.moment_y = moment_y
    column.theta_y = theta_y
    column.theta_ult = theta_ult
    column.force_y = column.moment_y / column.length
    column.disp_y = column.theta_y * column.length
    column.k = column.force_y / column.disp_y
    column.disp_u = theta_ult * column.length

    n_demands = 40
    delta_demand = np.linspace(0.0, 0.5, n_demands)

    grafico1 = funzione(0.5)
    grafico2 = funzione(0.7)
    grafico3 = funzione(0.9)
    grafico4 = funzione(1)
    plt.plot(delta_demand, grafico1, label="with reinforced_degree = 0.5")
    plt.plot(delta_demand, grafico2, label="with reinforced_degree = 0.7")
    plt.plot(delta_demand, grafico3, label="with reinforced_degree = 0.9")
    plt.plot(delta_demand, grafico4, label="with reinforced_degree = 1")
    # plt.axhline(column.disp_y, ls='--', color='green', label = "theta_y" )# TODO: change to show lines of the performance limits
    # plt.axhline(3/4*column.disp_u, ls='--', color='yellow',label ="3/4theta_ult" )
    # plt.axhline( column.disp_u, ls='--', color='red',label = "theta_ult")
    plt.grid(color='0.75', ls='--', lw=0.8)
    plt.legend('collapse')


    #plt.plot([0, 0.5], [0, 0.5], c="k")

    plt.ylim([0, 0.5])
    plt.xlim([0, max(delta_demand)])
    plt.title('diagram [ Disp_demands- Column_Disp]')
    plt.xlabel('disp_demands [ m ]')
    plt.ylabel('Column_Disp [m]')
    plt.legend()
    plt.savefig("First figure.png")
    plt.show()


if __name__ == '__main__':
    run()