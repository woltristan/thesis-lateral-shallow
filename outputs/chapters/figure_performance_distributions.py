
import matplotlib.pyplot as plt

from element_analysis import moment_theta as mt
from element_analysis import interaction_using_objects as interact
from element_analysis.models import Column, Soil
import numpy as np
from element_analysis import P_delta





def run():

    # Define Moment-theta
    h = 400  # mm
    b = 400  # mm
    c = 40  # mm
    f_ck = 25  # mPa
    f_yk = 450  # mPa
    phi = 18
    n_bars = 5  # mm
    d_bar = 16 + 12 * np.random.rand()
    l_1 = 3  # m
    l_2 = 4  # m
    n_storey = 3
    q = 10  # kPa
    shear_lenght = 4  # m
    spacing_stirrups = 150  # mm
    phi_stirrups = 8  # mm

    f_cd = mt.calc_f_cd(f_ck)
    f_yd = mt.calc_f_yd(f_yk)
    f_cm = mt.calc_f_cm(f_ck)
    As_tot = mt.calc_As_tot(b, h)
    Ab_s = mt.calc_Ab_s(d_bar, n_bars, As_tot)
    At_s = mt.calc_At_s(d_bar, n_bars, As_tot)
    f_ct = mt.calc_f_ct(f_ck)
    d = mt.calc_d(h, c)
    N = mt.calc_N(l_1, l_2, n_storey, q, f_ck, b, h)
    x = mt.calc_x(N, b, f_cd)
    E = mt.calc_E(f_cm)
    I = mt.calc_I(b, h)
    phi_y = mt.calc_phi_y(h, c, 0.00186)
    v = mt.calc_v(N, b, h, f_cd)
    omega_t = mt.calc_omega_t(At_s, f_yd, b, h, f_cd)
    omega_b = mt.calc_omega_b(At_s, f_yd, b, h, f_cd)
    b0 = mt.calc_b0(b, c, phi_stirrups, d_bar)
    h0 = mt.calc_h0(h, c, phi_stirrups, d_bar)
    alpha = mt.calc_efficience_stirrups(spacing_stirrups, b0, h0)
    area_stirrups = mt.calc_area_stirrups(phi_stirrups)
    pho_sx = mt.calc_geometrical_percentage_bars(area_stirrups, b0, spacing_stirrups)


    moment_y = mt.calc_M_y(f_cd,b,h,x,c,d,Ab_s,At_s,f_yd)
    theta_y = mt.calc_teta_y(phi_y,shear_lenght,h,f_yd,f_cd,phi)
    theta_ult = mt.calc_teta_u(v,omega_t,omega_b,f_cd,shear_lenght,h,alpha,pho_sx,f_yd)

    # Define the foundation
    depth_found = 1
    height_found = 0.5
    width = 1.2

    # Define the Soil
    phi = 33.
    gamma_soil = 18

    # Define Psoil
    phi_rad = np.radians(phi)
    k_passive = P_delta.calc_k_passive(phi_rad)
    p_p = P_delta.calc_p_p(gamma_soil, k_passive, depth_found, height_found)

    P_max = P_delta.calc_P_max(p_p, height_found, width)
    N_spt = P_delta.calc_N_spt(phi)
    k_soil = P_delta.calc_k_soil(N_spt, width)
    K_soil = P_delta.calc_K_soil(k_soil, width, depth_found)

    # Build the Soil Object
    soil = Soil()
    soil.k = K_soil
    soil.force_y = P_max
    soil.disp_y = P_max / K_soil

    # Build the Column Object
    column = Column()
    column.length = shear_lenght  # m
    column.moment_y = moment_y
    column.theta_y = theta_y
    column.theta_ult = theta_ult
    column.force_y = column.moment_y / column.length
    column.disp_y = column.theta_y * column.length
    column.k = column.force_y / column.disp_y

    disp_demands = np.linspace(0.00, 0.30, num=1000)
    column_disps = []
    for i in range(len(disp_demands)):
        disp_demand = disp_demands[i]

        column_disp = interact.solve_column_displacement_array(column, soil, disp_demand)
        print(column_disp)
        column_disps.append(column_disp)
        pl = calc_performance(column_disp / shear_lenght, theta_y, theta_ult)
        print(pl)

    force_array = np.linspace(0, 300, 1000)
    delta_soil_array = interact.calculate_delta_soil_plastic_array(soil, force_array, 2)
    delta_ss_array = interact.calculate_delta_ss_array(column, force_array, 2)
    delta_combo_array = delta_soil_array + delta_ss_array






def calc_k_ss(p_yield_column, disp_yield_column):
    return p_yield_column / disp_yield_column # the stiffness change due to the lenght


def calc_performance(theta_chord,theta_y,theta_ult):
    if theta_chord < theta_y:
        return " no damage"
    elif theta_y < theta_chord < 3/4 * theta_ult:
        return " life safety"
    elif theta_chord > 3/4 *theta_ult:
        return " collapse"


if __name__ == '__main__':
    run()







