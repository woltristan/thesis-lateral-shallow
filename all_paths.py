import os

ROOT_DIR = os.path.dirname(os.path.abspath(__file__)) + "/"  #  path to python project

LATERAL_SPREAD_DATA_PATH = ROOT_DIR + "lateral_spread_data/"