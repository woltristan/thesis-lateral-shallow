

class Column(object):
    length = 0.0  # m
    depth = 0.0  # m
    #epsilon_y = 0.00186
    moment_y = 0.0  # kNm
    theta_y = 0.0  # rad
    theta_ult = 0.0  # rad
    force_y = 0.0
    k = 0.0
    disp_y = 0.0
    moment_cr = 0.0


class Soil(object):
    disp_y = 0.0
    force_y = 0.0  # kN
    k = 0.0




