
import numpy as np
import matplotlib.pyplot as plt

import random
#MATERIALS PROPERTY ( C25/30==> fck= 25 mPa , B450C==> fyk=450 mPa)

def calc_f_cd (f_ck):
    """
    NTC 2008, Eq. 4.1.4
    """
    # f_cd = f_ck
    f_cd = 0.85 * f_ck / 1.5
    return f_cd

def calc_f_ct (f_ck):
    """
    NTC 2008, Eq. 11.2.3a
    """
    f_ct = 0.3 * f_ck ** (2/3)
    return f_ct

def calc_f_cm(f_ck):
    """
    NTC 2008, Eq. 11.2.2
    """
    f_cm = f_ck + 8
    return f_cm

def calc_E(f_cm):
    """
    NTC 2008, Eq. 11.2.5
    """
    E = 22000 * ((f_cm / 10) **0.3)
    return E

def calc_f_yd(f_yk):
    """
    NTC 2008, Eq. 4.1.6
    """
    # f_yd= f_yk
    f_yd = f_yk / 1.15
    return f_yd

#CROSS SECTION DATA (b,h= 400mm As,A's= 1271 mm**2 Asw= 157 mm**2 s=200 mm)

def calc_d(h,c):
    d = h - c
    return d

def calc_At_s(phi,n_bars, As_tot):
    """
    Areatop_steel
    """
    At_s = As_tot/2  #se vuoi questa mantienila accesa
    # At_s = 3.14 * n_bars * ((phi/2)**2)
    return At_s

def calc_Ab_s(phi,n_bars,As_tot):
    """
    Areabottom_steel
    """
    Ab_s = As_tot / 2   #se vuoi questa mantienila accesa
    # Ab_s = 3.14 * n_bars * ((phi/2)**2)
    return Ab_s
def calc_pho_longitudinal( At_s, Ab_s, b , h ):
    pho_longitudinal = (At_s+ Ab_s)/(b*h)
    return pho_longitudinal

def calc_I(b,h):
    """
    Total cross section Inertia
    """
    I = b * h**(3) / 12
    return I

def calc_v(N,b,h,f_cd):
    """
    dimensionless axial force
    """
    v = N * 10**3 / (b * h * f_cd)
    return v

def calc_omega_t(At_s,f_yd,b,h,f_cd):
    """
    mechanical top bar percentage
    """
    omega_t = At_s * f_yd / (b * h * f_cd)
    return omega_t

def calc_omega_b(Ab_s,f_yd,b,h,f_cd):
    """
    mechanical bottom bar percentage
    """
    omega_b = Ab_s * f_yd / (b * h * f_cd)
    return omega_b

#AXIAL FORCE

def calc_N(l_1,l_2,n_storey,q,f_ck,b,h):
    N = 0.6*(0.7*(6+ (f_ck - 15)/4)*b*h)/1000 #se vuoi computare con il tuo metodo oscura la seconda e funzionerà la prima
    # N = l_1 * l_2 * n_storey * q
    print(N, " sforzo normale ")
    return N

#M-teta

def calc_M_cr(b,h,f_ct,N):
    """
    first cracking
    """
    M_cr = (b * h**2 / 6 * (f_ct + ((N * 10**3) / (b * h))))/ 10**6
    return M_cr

def calc_theta_cr(M_cr,shear_lenght,E,I):
    """
    cracking rotation of fixed element at the top
    """
    theta_cr = M_cr * shear_lenght * 10**9 / (3 * E * I)
    return theta_cr

def calc_M_y(f_cd,b,h,x,c,d,Ab_s,At_s,f_yd):
    """
    yelding moment
    """

    M_y = (0.8 * b * f_cd * x * (h/2 - 0.4 * x ) + At_s * f_yd * (h/2 - c) + Ab_s * f_yd *(d - h/2)) / 10**6
    return M_y

def calc_x(N,b,f_cd):
    """
    stress block approach, bottom and top bars yelded, typical situation of column
    """
    x = N * 10**3 / ( 0.8 * b * f_cd)
    return x

def calc_phi_y(h,c,eps_sy):
    """
    phi_yelding, cross section deformation equilibrium
    """
    phi_y = 2 * eps_sy / (h - 2 * c)
    return phi_y

def calc_phi_u(eps_cu,x):
    """
    phi_u, cross section deformation equilibrium
    """
    phi_u = eps_cu / x
    return phi_u


def calc_teta_y(phy_y,shear_lenght,h,f_yd, f_cd, phi):
    """
    EC8, 8.7.2.1a
    """
    teta_y = phy_y * shear_lenght * 10**3 /3 + 0.0013 * (1 + (1.5 * h / (shear_lenght * 10**3))) + 0.13 * phy_y * phi * f_yd / np.sqrt(f_cd)
    return teta_y

def calc_b0 (b,c,phi_stirrups, phi):
    b0 = b - 2 * (c - phi_stirrups/2 - phi/2)
    return b0

def calc_h0(h, c, phi_stirrups, phi):
    h0 = h - 2 * (c - phi_stirrups / 2 - phi / 2)
    return h0

def calc_efficience_stirrups(spacing_stirrups,b0,h0):
    alpha = (1 - spacing_stirrups/(2 * b0)) * (1 - spacing_stirrups/(2 * h0)) * (1 - (((2 * b0**2) + (2 * h0**2)) / (6 * h0 * b0)))
    return alpha

def calc_area_stirrups(phi_stirrups):
    area_stirrups = 3.14 * phi_stirrups**2 / 4
    return area_stirrups

def calc_geometrical_percentage_bars( area_stirrups,b0,spacing_stirrups ):
    pho_sx = 2 * area_stirrups / (b0 * spacing_stirrups)
    return pho_sx

def calc_geometrical_percentage_bars_longitudinal(phi, n_bars):
    pho_longitudinal =  3.14 * n_bars * ((phi/2)**2)
    return pho_longitudinal

def calc_teta_u(v,omega_t,omega_b,f_cd,shear_lenght,h,alpha,pho_sx,f_yd):
    """
    EC8 A.3.2.2,
    """
    teta_u = 0.016 * 0.3**v * (max(0.01,omega_t) / max(0.01,omega_b) * f_cd)**0.225 * (shear_lenght * 10**3 / h)**0.35 * 25**(alpha * pho_sx * f_yd/f_cd)
    return teta_u

def calc_M_u(M_y):
    """
    HP M_yelding = M_ultimate
    """
    M_u = M_y
    return M_u
def calc_As_tot(b,h):
    As_tot =  (0.01 * (b * h))
    # As_tot = random.uniform(0.005,0.01)* ( b* h)
    return As_tot




def calculate_m_theta():
    #Data

    h = 400 # mm
    b = 400  # mm
    c = 40   # mm
    f_ck = 25 #mPa
    f_yk = 450 #mPa
    phi = 18
    n_bars = 5  # mm
    l_1 = 3  # m
    l_2 = 4  # m
    n_storey = 3
    q = 10  # kPa
    shear_lenght = 4  # m
    spacing_stirrups = 150 #mm
    phi_stirrups = 8 #mm
    As_tot = calc_As_tot(b,h)
    print(As_tot)
    f_cd = calc_f_cd(f_ck)
    f_yd = calc_f_yd(f_yk)
    f_cm = calc_f_cm(f_ck)
    Ab_s = calc_Ab_s(phi,n_bars,As_tot)
    At_s = calc_At_s(phi,n_bars,As_tot)
    f_ct = calc_f_ct(f_ck)
    d = calc_d(h, c)

    N = calc_N(l_1,l_2,n_storey,q,f_ck,b,h)
    print(N)
    M_cr = calc_M_cr(b, h, f_ct, N)
    x = calc_x(N, b, f_cd)
    M_y = calc_M_y(f_cd,b,h,x,c,d,Ab_s,At_s,f_yd)
    E = calc_E(f_cm)
    I = calc_I(b,h)
    theta_cr = calc_theta_cr(M_cr,shear_lenght,E,I)
    phi_y = calc_phi_y(h,c,0.00186)
    teta_y = calc_teta_y(phi_y,shear_lenght,h,f_yd,f_cd,phi)
    v = calc_v(N,b,h,f_cd)
    omega_t = calc_omega_t(At_s,f_yd,b,h,f_cd)
    omega_b = calc_omega_b(At_s, f_yd, b, h, f_cd)
    b0 = calc_b0(b,c,phi_stirrups,phi)
    h0 = calc_h0(h,c,phi_stirrups,phi)
    alpha = calc_efficience_stirrups(spacing_stirrups,b0,h0)
    area_stirrups = calc_area_stirrups(phi_stirrups)
    pho_sx = calc_geometrical_percentage_bars(area_stirrups,b0,spacing_stirrups)
    teta_u = calc_teta_u(v,omega_t,omega_b,f_cd,shear_lenght,h,alpha,pho_sx,f_yd)
    M = [0, M_cr, M_y, M_y]
    theta = [0, theta_cr, teta_y, teta_u]
    pho_longitudinal = calc_pho_longitudinal( At_s,Ab_s, b, h)
    print(pho_longitudinal)
    return theta, M

def run():
    theta, M = calculate_m_theta()
    plt.plot(theta, M, label="M-THETA")
    plt.title('diagram [ M - THETA ]')
    plt.xlabel('theta [ - ]')
    plt.ylabel('M [ KNm]')
    plt.legend()
    plt.show()


if __name__ == '__main__':
    run()
