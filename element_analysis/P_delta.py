import numpy as np
import matplotlib.pyplot as plt


def calc_k_passive(phi):
    k_passive = (1 + np.sin(phi)) / (1 - np.sin(phi))
    return k_passive


def calc_k_active(phi):
    k_passive = (1 - np.sin(phi)) / (1 + np.sin(phi))

    return k_passive


def calc_p_p(gamma_soil, k_passive, depth_found, height_found):
    # show the reason
    p_p = gamma_soil * k_passive * (depth_found - height_found / 2)
    return p_p
def calc_K_alternative(p_p,height_found,width,delta_u_alternative):
    K_alternative = 4.5*p_p*height_found*width/delta_u_alternative
    return K_alternative
def calc_delta_u_alternative(depth_found):
    delta_u_alternative = 0.05*depth_found
    return delta_u_alternative



def calc_P_max(p_p, height_found, width):
    alpha_c = 4.5
    P_max = alpha_c * p_p * height_found * width
    return P_max


def calc_N_spt(phi):
    # dal file sui ponti in nuova zelanda c'è una ralzione tra phi (a cui abbiamo dato una distribuzione normale) e N allora ho imposto questa relazione
    # q_c = 1.65
    q_c = 0.65
    I_c = 2.69
    # N_spt = (q_c / 0.1) / (8.5 * (1 - (I_c / 4.6)))
    N_spt = (phi - 20) ** 2 / 20
    return N_spt


def calc_k_soil(N_spt, width):
    # k_soil = (56 * N_spt * (100 * width) ** (-3 / 4)) * 1000
    k_soil = 0.3*(56 * N_spt * (100 * width) ** (-3 / 4)) * 1000 #dal file sui ponti è scritt che per computare la non linearità si può usare un coefficiente beta di 0.3 per abbssare il k
    return k_soil


def calc_K_soil(k_soil, width, depth_found):
    K_soil = k_soil * width * depth_found
    return K_soil


def run():
    for i in range(1):
        phi = 33.
        phi_rad = np.radians(phi)
        k_passive = calc_k_passive(phi_rad)
        depth_found = 1
        height_found = 0.5
        p_p = calc_p_p(18, k_passive, depth_found,height_found)
        width = 1.2

        P_max = calc_P_max(p_p, height_found, width)
        N_spt = calc_N_spt(phi)
        k_soil = calc_k_soil(N_spt, width)
        K_soil = calc_K_soil(k_soil, width, depth_found)
        delta_u_alternative = calc_delta_u_alternative(depth_found)
        K_soil_alternative = calc_K_alternative(p_p,height_found,width,delta_u_alternative)
        print(K_soil_alternative)
        print(N_spt)
        print(k_soil)
        print("width: ", width)
        print("depth_found: ",depth_found)
        print("P_max:",P_max)
        print("delta_soil_y:",P_max / K_soil)

        P = [0, P_max, P_max]
        print("N_spt:",N_spt)

        delta_soil_y = P_max/K_soil
        print(P_max / delta_soil_y, "rigidezza variatta")
        delta_soil_y_cubrinosky = 0.05*depth_found
        Delta = [0, delta_soil_y, delta_soil_y * 1.5]
        delta_cub = [0,delta_soil_y_cubrinosky,delta_soil_y_cubrinosky*1.5]
        plt.plot(delta_cub,P)
        plt.plot(Delta, P, label="P-Delta")
        plt.title('diagram [ P-Delta ]')
        plt.xlabel('Delta [ m ]')
        plt.ylabel('P [ KN]')
        plt.legend()
        plt.show()




if __name__ == '__main__':
    run()
