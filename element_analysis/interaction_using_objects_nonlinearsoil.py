from element_analysis.models import Column, Soil
import numpy as np
import matplotlib.pyplot as plt








def calc_k_ss(p_yield_column, disp_yield_column):
    return p_yield_column / disp_yield_column # the stiffness change due to the lenght


def calculate_delta_soil_array(soil, force_array, disp_max):
    # assume force is an array
    return np.where(force_array < soil.force_y, force_array / soil.k, disp_max)


def calculate_delta_soil_nonlinear_array(soil, force_array):
    # assume force is an array
    return force_array / (soil.k * (1 - (force_array/(1.2*soil.force_y))))


def calculate_delta_ss_array(column, force_array, disp_max):
    col_disp = np.where(force_array < column.force_y, force_array / column.k, disp_max)
    return col_disp

def calculate_delta_strain_hardening_ss_array(column, force_array, disp_max):

    col_disp = np.where(force_array < column.force_y, force_array / column.k, (force_array - column.force_y) / (0.05 * column.k) + column.disp_y)

    return col_disp


def calculate_force_demand(force_array, delta_combo_array, delta_demand):
    return np.interp(delta_demand, delta_combo_array, force_array)


def solve_column_displacement_array(column, soil, disp_demand):
    force_array = np.linspace(0, 600, 10000)  # crea un array che parte da 0 fino a 600 e è formato da 10000 valori
    delta_soil_array = calculate_delta_soil_nonlinear_array(soil, force_array)
    delta_ss_array = calculate_delta_ss_array(column, force_array, 2)
    delta_combo_array = delta_soil_array + delta_ss_array
    # Shift force array by 1 to account for array based method not capturing true yield force.
    force_array1 = force_array[1:]
    delta_combo_array1 = delta_combo_array[:-1]
    p_demand = calculate_force_demand(force_array1, delta_combo_array1, disp_demand)
    # p_demand = np.array([p_demand])
    if p_demand > soil.force_y:
        delta_ss = calculate_delta_ss_array(column, p_demand, disp_demand)
        return delta_ss
    else:
        disp_soil = calculate_delta_soil_nonlinear_array(soil, force_array)
        delta_ss = calculate_delta_ss_array(column, p_demand, disp_demand - disp_soil)
        print("dd: ", disp_soil, delta_ss)
        return delta_ss


def solve_column_displacement_strain_hardening_array(column, soil, disp_demand):
    force_array = np.linspace(0, 600, 10000)
    delta_soil_array = calculate_delta_soil_nonlinear_array(soil, force_array)
    # delta_ss_array = calculate_delta_ss_array(column, force_array, 2)
    delta_ss_array = calculate_delta_strain_hardening_ss_array(column, force_array, 2)
    delta_combo_array = delta_soil_array + delta_ss_array
    # Shift force array by 1 to account for array based method not capturing true yield force.
    force_array1 = force_array[1:]
    delta_combo_array1 = delta_combo_array[:-1]
    p_demand = calculate_force_demand(force_array1, delta_combo_array1, disp_demand)
    # p_demand = np.array([p_demand])
    if p_demand < soil.force_y:
        # delta_ss = calculate_delta_ss_array(column, p_demand, disp_demand)
        delta_ss = calculate_delta_strain_hardening_ss_array(column, p_demand, disp_demand)
        return delta_ss
    else:
        disp_soil = calculate_delta_soil_nonlinear_array(soil, force_array)
        # delta_ss = calculate_delta_ss_array(column, p_demand, disp_demand - disp_soil)
        delta_ss = calculate_delta_strain_hardening_ss_array(column, p_demand, disp_demand - disp_soil)
        return delta_ss




def solve_column_displacement(column, soil, disp_demand):
    """
    Calculates the displacement of the structure
    :param column:
    :param soil:
    :param disp_demand:
    :return:
    """
    # Assume column displacement is equal to demand
    # Calculate stiffnesses
    p_yield_column = column.moment_y / column.length
    disp_y_column = column.theta_y * column.length
    column_disp_yield = column.theta_y * column.length
    column_k = calc_k_ss(p_yield_column, column_disp_yield)
    # soil_k is a costant
    soil_k = soil.force_y / soil.disp_y
    soil_disp = p_yield_column / soil_k  # Step 3

    column_disp = disp_demand - soil_disp
    if soil_disp < soil.disp_y and column_disp > 0:
        for i in range(10):
            column_disp = disp_demand - soil_disp
            if column_disp > disp_y_column:
                p_column = p_yield_column
            else:
                p_column = column_k * column_disp
            soil_disp = p_column / soil_k
        p_soil = p_column
        soil_disp = p_soil / soil_k
        column_disp = disp_demand - soil_disp
        return column_disp
    elif soil_disp > soil.disp_y and column_disp > 0:  # case 2
        column_disp = soil.force_y / column_k
        return column_disp
    elif soil_disp < soil.disp_y and column_disp < 0:  # case 3
        temp_soil_disp = 0.2

        for i in range(30):

            p_soil = soil_k * temp_soil_disp
            column_disp = p_soil / column_k
            soil_disp = disp_demand - column_disp
            if abs(temp_soil_disp - soil_disp) < 0.0001:
                return column_disp
            else:
                temp_soil_disp = soil_disp


def run():

    column = Column()
    column.length = 4.  # m
    column.moment_y = 220.
    column.theta_y = 0.004435
    column.theta_ult = 0.054

    # Define the Soil
    soil = Soil()
    soil.disp_y = 0.3
    soil.force_y = 70.

    delta_demand = 0.25  # m

    disp_of_column = solve_column_displacement(column, soil, delta_demand)
    print(disp_of_column)


def run_case1():

    column = Column()
    column.length = 4.  # m
    column.moment_y = 220.
    column.theta_y = 0.00582
    column.theta_ult = 0.0537

    # Define the Soil
    soil = Soil()
    soil.disp_y = 0.3
    soil.force_y = 70.

    delta_demand = 0.4  # m

    disp_of_column = solve_column_displacement(column, soil, delta_demand)
    expected_column_disp = 0.164
    assert 0.99 < disp_of_column / expected_column_disp < 1.01
    print(disp_of_column)


def run_case2():

    column = Column()
    column.length = 3.  # m
    column.moment_y = 220.
    column.theta_y = 0.00437
    column.theta_ult = 0.0486
    column.force_y = column.moment_y / column.length
    disp_y = column.theta_y * column.length
    column.k = column.force_y / disp_y

    # Define the Soil
    soil = Soil()
    soil.disp_y = 0.3
    soil.force_y = 70.
    soil.k = soil.force_y / soil.disp_y

    delta_demand = 0.4  # m

    disp_of_column = solve_column_displacement_array(column, soil, delta_demand)
    expected_column_disp = 0.0125  # TODO: UPDATE THIS VALUE
    print(disp_of_column)
    assert 0.949 < disp_of_column / expected_column_disp < 1.051


def run_case3():
    column = Column()
    column.length = 4.  # m
    column.moment_y = 220.
    column.theta_y = 0.00582
    column.theta_ult = 0.0537
    column.force_y = column.moment_y / column.length
    disp_y = column.theta_y * column.length
    column.k = column.force_y / disp_y

    # Define the Soil
    soil = Soil()
    soil.disp_y = 0.3
    soil.force_y = 70.
    soil.k = soil.force_y / soil.disp_y

    delta_demand = 0.2  # m

    disp_of_column = solve_column_displacement_array(column, soil, delta_demand)
    expected_column_disp = 0.018  # TODO: UPDATE THIS VALUE
    print(disp_of_column)
    assert 0.99 < disp_of_column / expected_column_disp < 1.01


def run_random_cases():

    column = Column()
    column.length = 3.5  # np.random.random((2,4))  # m
    column.moment_y = 220.
    column.theta_y = 0.00582
    column.theta_ult = 0.0537

    # Define the Soil
    soil = Soil()
    soil.disp_y = 0.3
    soil.force_y = 70.

    delta_demand =np.random.random()
    print(delta_demand)# m

    disp_of_column = solve_column_displacement(column, soil, delta_demand)

    print(disp_of_column)


if __name__ == '__main__':
    run_case1()
    run_case2()
    run_case3()
    run_random_cases()