import numpy as np
import matplotlib.pyplot as plt
from element_analysis import Moment_teta as mt
from element_analysis import P_delta as pd

def calc_M_b(P_str,L):
    M_b = P_str * L *10**6
    return M_b

def calc_inertia_cracking(b,h):
    I_1 = b * h**3 / 12
    return I_1

def calc_inertia_yielding(I_1):
    I_2 = 0,75 * I_1
    return I_2

# HP: delta_diff = delta_ss

def calc_disp_diff(M_b,M_c,M_y,E,I_1,I_2,L):
    if M_b < M_c:
        return (M_b * L  / (3 * E * I_1)) / 10**3
    if M_c <= M_b <= M_y:
        return (M_b * L / (3 * E * I_2)) / 10**3



def run():

    theta, M = mt.calculate_m_theta()
    teta_y = mt.calc_teta_y()
    theta_cr = mt.calc_theta_cr()
    M_cr = mt.calc_M_cr()
    M_y = mt.calc_M_y()
    M_u = mt.calc_M_u()
    teta_u = mt.calc_teta_u()
    E = mt.calc_E()
    I_1 = calc_inertia_cracking(400,400)
    p = np.arange(0, 54)
    M_b = calc_M_b(p, 4)


if __name__ == '__main__':
    # run()


    # M = [0, M_cr, M_y, M_y]
    # theta = [0, theta_cr, teta_y, teta_u]

    theta, M = mt.calculate_m_theta()
    P = [0,54,54]
    delta_diff = [0,0.3,2]

    plt.plot(delta_diff, P, label="p-delta")
    #plt.plot(theta, M, label="p-delta")


    plt.legend()
    plt.show()