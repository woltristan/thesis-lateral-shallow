import numpy as np
import matplotlib.pyplot as plt


def calc_k_passive(phi):
    k_passive = (1 + np.sin(phi)) / (1 - np.sin(phi))
    return k_passive


def calc_k_active(phi):
    k_passive = (1 - np.sin(phi)) / (1 + np.sin(phi))

    return k_passive


def calc_p_p(gamma_soil, k_passive, depth_found, height_found):
    # show the reason
    p_p = gamma_soil * k_passive * (depth_found - height_found / 2)
    return p_p


def calc_P_max(p_p, height_found, width):
    alpha_c = 4.5
    P_max = alpha_c * p_p * height_found * width
    return P_max


def calc_N_spt(phi):
    # dal file sui ponti in nuova zelanda c'è una ralzione tra phi (a cui abbiamo dato una distribuzione normale) e N allora ho imposto questa relazione

    N_spt = (phi - 20)**2/20
    return N_spt


def calc_k_soil(N_spt, width):
    k_soil = (56 * N_spt * (100 * width) ** (-3 / 4)) * 1000
    return k_soil


def calc_K_soil(k_soil, width, depth_found):
    K_soil = k_soil * width * depth_found
    return K_soil


def delta_soil_nonlinear(P_nonlinear, K_soil, P_max):
    Delta_soil_nonlinear = np.zeros(len(P_nonlinear))
    for i in range(0, len(P_nonlinear)):
        Delta_soil_nonlinear[i] = P_nonlinear[i] / (K_soil*(1-P_nonlinear[i]/(1.2*P_max)))
    return Delta_soil_nonlinear


def P_nonlinear(number, P_max):
    step = P_max/number
    array = np.zeros(number)

    for i in range(1, number):
        array[i] = array[i-1] + step
    return array


def run():
    for i in range(1):
        phi = 33.
        phi_rad = np.radians(phi)
        k_passive = calc_k_passive(phi_rad)
        depth_found = 1
        height_found = 0.5
        p_p = calc_p_p(18, k_passive, depth_found,height_found)
        width = 1.2

        P_max = calc_P_max(p_p, height_found, width)
        N_spt = calc_N_spt(phi)
        k_soil = calc_k_soil(N_spt, width)
        K_soil = calc_K_soil(k_soil, width, depth_found)

        print(N_spt)
        print(k_soil)
        print("width: ", width)
        print("depth_found: ",depth_found)
        print("P_max:",P_max)
        print("delta_soil_y:",P_max / K_soil)

        number = 150

        P = [0, P_max, P_max]
        p_nonlinear = P_nonlinear(number, P_max)
        delta_non_linear = delta_soil_nonlinear(p_nonlinear, K_soil, P_max)
        print(delta_non_linear)

        plt.plot(delta_non_linear, p_nonlinear, label="P-Delta non linear " )
        #plt.show()
        delta_soil_y = P_max / K_soil
        Delta = [0, delta_soil_y, delta_soil_y*5.8]
        plt.plot(Delta, P, label="P-Delta bilinear")
        plt.title('diagram [ P-Delta ]')
        plt.xlabel('Delta [ m ]')
        plt.ylabel('P [ KN]')
        plt.legend()
        plt.show()




if __name__ == '__main__':
    run()
