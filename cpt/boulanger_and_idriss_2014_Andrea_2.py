import numpy as np
import matplotlib.pyplot as plt
from cpt import shear_strain

def calculate_sigma_v(depths, gammas):
    """
    Calculates the vertical stress
    """
    depth_incs = depths[1:] - depths[:-1]
    depth_incs = np.insert(depth_incs, 0, 0)
    sigma_v_incs = depth_incs * gammas
    sigma_v = np.cumsum(sigma_v_incs)
    return sigma_v


def crr_7p5_from_cpt(q_c1n_cs):
    """
    cyclic resistance from CPT, Eq. 2.24
    """
    crr = np.exp((q_c1n_cs / 113) + ((q_c1n_cs / 1000) ** 2) - ((q_c1n_cs / 140) ** 3) + ((q_c1n_cs / 137) ** 4) - 2.8)
    return crr


def calculate_delta_q_c1n(q_c1n, fc):
    """
    delta q_c1n from CPT, Eq 2.22
    """
    delta_q_c1n = (11.9 + (q_c1n / 14.6)) * (np.exp(1.63 - (9.7 / (fc + 2)) - ((15.7 / (fc + 2)) ** 2)))
    return delta_q_c1n


def calculate_q_c1ncs(q_c1n, delta_q_c1n):
    """
    q_c1ncs from CPT, Eq 2.10
    """
    q_c1ncs = q_c1n + delta_q_c1n
    return q_c1ncs

def calculate_rd(depth, magninitude):
    """
    rd from CPT, Eq 2.14a
    """
    alpha = -1.012 - 1.126 * np.sin((depth / 11.73) + 5.133)
    beta = 0.106 + 0.118 * np.sin((depth/11.28) + 5.142)
    rd=np.exp(alpha+beta*magninitude)
    return rd

def calculate_pore_pressure(depth, gwl):
    gamma_water=10
    pore_pressure = np.where(depth > gwl, (depth - gwl) * gamma_water, 0.0)
    return pore_pressure

def calculate_sigma_veff(sigma_v, pore_pressure):
    sigma_veff = sigma_v - pore_pressure
    return  sigma_veff

def calculate_CSR(sigma_veff, sigma_v, pga, rd):
    """
    Cyclic stress ratio from CPT, Eq 2.2
    """
    CSR=0.65*(sigma_v/sigma_veff)*rd*pga
    return CSR

def calculate_CN(m,sigma_veff):
    """
    CN parameter from CPT, Eq 2.15a
    """
    CN=(100/sigma_veff)**m
    if CN>1.7:
        CN=1.7
    return CN

def calculate_m(q_c1ncs):
    """
    m parameter from CPT, Eq 2.15b
    """
    m=1.338-0.249*q_c1ncs**0.264
    return m

def calculate_q_c1n(qc, CN):
    """
    qc1n from CPT, Eq 2.4
    """
    q_c1n=CN*qc*1000/100
    return q_c1n

#from zhang document, LDI calculation

# Relative density, Eq.2
def calculate_D_r(q_c1n,fc):

    D_r = np.where(fc >0.65, -100, - 85 + 76 * np.log10(q_c1n)) / 100
    return D_r


def calculate_delta_z(depth):
    delta_z = depth[1:] - depth[:-1]
    delta_z = np.insert(delta_z, 0, 0)
    return delta_z

def calculate_LDI(gamma_max, delta_z):
    num_depth=len(delta_z)
    ldi= gamma_max * delta_z
    return np.cumsum(ldi[::-1])[::-1]
    # LDI = np.zeros(num_depth)
    # for i in range(0, num_depth):
    #     LDI[i] = LDI[i - 1] + ldi[i]
    # return LDI

def calculate_LD(LDI):
    ld= 0.4 *LDI
    return ld



def calculate_fs(fname, gwl=0.94, pga=0.2, magnitude=7.5):
    gamma_water = 10.
    p_a = 100.  # kPa

    # import data from csv file
    data = np.loadtxt(fname, skiprows=2,  usecols=(0, 2, 9, 21))
    depth = data[:, 0]
    q_c = data[:, 1]  # kPa
    gammas = data[:, 2]
    fines_content = data[:, 3]

    num_depth = len(depth)

    sigma_v = calculate_sigma_v(depth, gammas)

    # plot versus aurelio's values
    # xlsx_sigma_v = np.loadtxt(fname, skiprows=1, delimiter=",", usecols=10)
    # plt.plot(sigma_v, -depth, label="python")
    # plt.plot(xlsx_sigma_v, -depth, ls="--", label="xlsx")
    # plt.legend()
    # plt.show()
    pore_pressure = calculate_pore_pressure(depth, 0.94)
    sigma_veff = calculate_sigma_veff(sigma_v,pore_pressure)
    rd=calculate_rd(depth,7.5)
    CSR=calculate_CSR(sigma_veff, sigma_v,0.2,rd)


    m_values = np.ones(num_depth)  # create an array of ones
    cn_values = np.zeros(num_depth)
    q_c1n = np.zeros(num_depth)
    delta_q_c1n = np.zeros(num_depth)
    q_c1n_cs = np.zeros(num_depth)

    for i in range(0, num_depth):
        for j in range(20):
            cn_values[i] = min((p_a / sigma_veff[i]) ** m_values[i], 1.7)  # Eq 2.15a
            temp_cn = cn_values[i]
            q_c1n[i] = cn_values[i] * q_c[i] / p_a  # Eq. 2.4

            delta_q_c1n[i] = calculate_delta_q_c1n(q_c1n=q_c1n[i], fc=fines_content[i])  # Eq. 2.22
            q_c1n_cs[i] = q_c1n[i] + delta_q_c1n[i]
            m_values[i] = calculate_m(q_c1n_cs[i])
            if abs(q_c1n[i] - temp_cn) < 0.001:
                break

    # CN=calculate_CN(calculate_m(calculate_q_c1ncs(calculate_q_c1n(q_c), calculate_delta_q_c1n(calculate_q_c1n(q_c),fines_content))))
    CRR=crr_7p5_from_cpt(q_c1n_cs)
    fs_uncorrected = (CRR / CSR)
    # fs = fs_uncorrected
    # for j, fc in enumerate(fines_content):
    #    if fc>70:
    #        fs[j] = 2

    fs = np.where(fines_content > 70, 2.0, fs_uncorrected)
    dr=calculate_D_r(q_c1n,fines_content)

    gamma_max=shear_strain.calculate_array_shear_strain(fs, dr)
    delta_z=calculate_delta_z(depth)
    ldi= calculate_LDI(gamma_max,delta_z)
    ld = calculate_LD(ldi)

    plt.title('diagram Lateral Displacement')
    plt.xlabel('Ug [ m ]')
    plt.ylabel('Depth [ m ]')
    plt.legend()
    plt.plot(ld, -depth, label="LD, Depth")


    #csv_values = np.loadtxt(fname, skiprows=2, delimiter=";", usecols=42)
    #plt.plot(LPI, -depth, label="python")
    #plt.plot(csv_values, -depth, ls="--", label="csv")
    plt.legend()
    plt.show()

    factor_safety = np.ones(num_depth)

    return depth, factor_safety





if __name__ == '__main__':
    cpt_file = 'definitivo2.csv'
    depth, factor_safety = calculate_fs(cpt_file)
