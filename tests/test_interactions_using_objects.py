from element_analysis import interaction_using_objects as iuo
from element_analysis.models import Column, Soil


def test_run_case1():

    column = Column()
    column.length = 4.  # m
    column.moment_y = 220.
    column.theta_y = 0.00582
    column.theta_ult = 0.0537

    # Define the Soil
    soil = Soil()
    soil.disp_y = 0.3
    soil.force_y = 70.

    delta_demand = 0.4  # m

    disp_of_column = iuo.solve_column_displacement(column, soil, delta_demand)
    expected_column_disp = 0.164
    assert 0.99 < disp_of_column / expected_column_disp < 1.01
    print(disp_of_column)


def test_run_case2():

    column = Column()
    column.length = 3.  # m
    column.moment_y = 220.
    column.theta_y = 0.00437
    column.theta_ult = 0.0486

    # Define the Soil
    soil = Soil()
    soil.disp_y = 0.3
    soil.force_y = 70.

    delta_demand = 0.4  # m

    disp_of_column = iuo.solve_column_displacement(column, soil, delta_demand)
    expected_column_disp = 0.0125  # TODO: UPDATE THIS VALUE
    print(disp_of_column)
    assert 0.99 < disp_of_column / expected_column_disp < 1.01


def test_run_case3():
    column = Column()
    column.length = 4.  # m
    column.moment_y = 220.
    column.theta_y = 0.00582
    column.theta_ult = 0.0537

    # Define the Soil
    soil = Soil()
    soil.disp_y = 0.3
    soil.force_y = 70.

    delta_demand = 0.2  # m

    disp_of_column = iuo.solve_column_displacement(column, soil, delta_demand)
    expected_column_disp = 0.018  # TODO: UPDATE THIS VALUE
    print(disp_of_column)
    assert 0.99 < disp_of_column / expected_column_disp < 1.01
