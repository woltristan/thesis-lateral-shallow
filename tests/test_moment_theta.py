
from element_analysis import moment_theta as mt


def test_calc_phi_u():
    x = 10
    eps_cu = 20

    phi_u = mt.calc_phi_u(eps_cu, x)
    expected_phi_u = 2
    assert expected_phi_u == phi_u


